import java.util.Scanner;

public class Main {

    static String[] hangman = new String[5];

    static String word = "cat";

    static String guessedWord = "";

    static char[] guessedWordArr = new char[word.length()];

    public static void main(String[] args) {

        String playAgain = "yes";

        while (playAgain.equals("yes")) {

            hangman[0] = "+---+\n";
            hangman[1] = "    |\n";
            hangman[2] = "    |\n";
            hangman[3] = "    |\n";
            hangman[4] = "   ===\n";

            drawHangman();
            System.out.println("Missed letters");
            printGuessedLetters();

            Scanner scanner = new Scanner(System.in);

            int wrongCounter = 0;
            int correctGuesses = 0 ;



            for (int i = 0; i <= hangman.length; i++) {
                System.out.println("\nGuess a letter.");
                char c = scanner.next().charAt(0);

                if (word.contains("" + c)) {
                    int index = word.indexOf("" + c);
                    guessedWordArr[index] = c;
                    correctGuesses++;
                    drawHangman();

                    if(correctGuesses == word.length())
                    {
                        System.out.println("Yes! The secret word is \""+ word+ "\"! You have won!");
                        break;
                    }


                } else {
                    wrongCounter++;

                    updateHangman(wrongCounter);

                    drawHangman();
                }

                for (int j = 0; j < guessedWordArr.length; j++) {
                    System.out.print(guessedWordArr[j] + " ");
                }

            }

            System.out.println("Do you want to play again? (yes or no)");
            playAgain = scanner.next();

        }


    }

    public static void printGuessedLetters()
    {
        if(guessedWord.equals(""))
        {
            for(int i=0 ; i< word.length(); i++)
            {
                guessedWordArr[i] = '_';
                System.out.print("_ ");
            }

        }

    }

    public static void updateHangman(int index)
    {

        switch (index)
        {
            case 1:
                hangman[1] = "O   |\n";
                break;

            case 2:
                hangman[2] = "|   |\n";
                break;

            case 3:
                hangman[3] = "|   |\n";
                break;

            case 4:
                hangman[4] = "/  ===\n";
                break;

            case 5:
                hangman[4] = "/\\ ===\n";
                break;

        }


    }

    public static void drawHangman()
    {
        for (int i=0; i<hangman.length; i++)
        {
            System.out.print(hangman[i]);
        }
    }
}
